import os


def get_driver_path(browser_name):
    if browser_name == "Chrome":
        return os.path.join(os.path.dirname(__file__), "..", "drivers", "chromedriver.exe")
    elif browser_name == "Firefox":
        return os.path.join(os.path.dirname(__file__), "..", "drivers", "geckodriver.exe")
