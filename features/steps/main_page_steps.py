from behave import *
from pages.base_page import BasePage
from pages.login_page import LoginPage
from pages.main_page import MainPage


@given("I am logged in")
def step_impl(context):
    context.execute_steps("""
    Given I am on login page
    When I log in with correct credentials
    """)


@when("I log out")
def step_impl(context):
    page = MainPage(context.driver)
    page \
        .logout()
    page = LoginPage(context.driver)
    context.expected_url = page.login_url


@when("I choose My Shortcuts menu option {option}")
def step_impl(context, option):
    page = MainPage(context.driver)
    page \
        .click_my_shortcuts_menu(option)
    context.expected_url = page.get_expected_url(option)


@when("I choose Admin - User Management menu option {option}")
def step_impl(context, option):
    page = MainPage(context.driver)
    page \
        .click_admin_user_management_menu(option)
    context.expected_url = page.get_expected_url(option)


@then("I should see correct page url")
def step_impl(context):
    page = BasePage(context.driver)
    assert context.expected_url == page.get_url()
