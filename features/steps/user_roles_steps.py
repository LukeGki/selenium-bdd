from behave import *
from pages.user_roles_page import UserRolesPage
from pages.user_role_page import UserRolePage


@given("I am on User Roles page")
def step_impl(context):
    page = UserRolesPage(context.driver)
    page \
        .load()


@given("I added new User Role")
def step_impl(context):
    # should use Requests or MySQL Connector
    context.execute_steps("""
    When I open Add User Role
    And I fill correct Add User Role data
    And I click User Role Save button
        """)


@when("I open Add User Role")
def step_impl(context):
    page = UserRolesPage(context.driver)
    page \
        .click_add_user_role_button()


@when("I open Edit User Role")
def step_impl(context):
    page = UserRolesPage(context.driver)
    page \
        .click_user_role_edit(context.user_role_name)


@when("I fill correct Add User Role data")
def step_impl(context):
    page = UserRolePage(context.driver)
    context.user_role_type = page.new_user_role_type
    context.user_role_name = page.new_user_role_name
    page \
        .fill_add_user_role_data(context.user_role_type, context.user_role_name)


@when("I fill correct Edit User Role data")
def step_impl(context):
    page = UserRolePage(context.driver)
    context.user_role_name_edit = page.new_user_role_name_edit
    page \
        .edit_user_role_name(context.user_role_name_edit)
    context.user_role_name = context.user_role_name + context.user_role_name_edit


@when("I click User Role Save button")
def step_impl(context):
    page = UserRolePage(context.driver)
    page \
        .click_save_button()


@when("I select User Role checkbox")
def step_impl(context):
    page = UserRolesPage(context.driver)
    page \
        .click_user_role_checkbox(context.user_role_name)


@when("I delete selected rows")
def step_impl(context):
    page = UserRolesPage(context.driver)
    page \
        .choose_table_option_delete_selected() \
        .confirm_table_option_delete_selected()


@when("I delete User Role")
def step_impl(context):
    # should use Requests or MySQL Connector
    context.execute_steps("""
    When I select User Role checkbox
    And I delete selected rows
        """)


@then("I should see validation message")
def step_impl(context):
    page = UserRolePage(context.driver)
    assert page.is_user_role_name_required_validation_message_displayed() == True


@then("I should see correct data in table")
def step_impl(context):
    page = UserRolesPage(context.driver)
    assert page.check_table_row_present(context.user_role_name, context.user_role_type) == True


@then("I should not see given data in table")
def step_impl(context):
    page = UserRolesPage(context.driver)
    assert page.check_table_row_present(context.user_role_name, context.user_role_type) == False
