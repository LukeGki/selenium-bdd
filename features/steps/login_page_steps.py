from behave import *
from pages.login_page import LoginPage
from pages.main_page import MainPage


@given("I am on login page")
def step_impl(context):
    page = LoginPage(context.driver)
    page \
        .load()


@when("I log in with correct credentials")
def step_impl(context):
    page = LoginPage(context.driver)
    page \
        .login()


@then("I should see correct page title")
def step_impl(context):
    page = LoginPage(context.driver)
    assert page.title == page.get_title()


@then("I should see correct username on page")
def step_impl(context):
    page = MainPage(context.driver)
    assert page.username == page.get_username()
