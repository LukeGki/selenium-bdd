import allure
from allure_commons.types import AttachmentType
from selenium import webdriver
from configuration import config_loader, driver_loader


def before_all(context):
    configuration = config_loader.load_configuration()
    context.browser = configuration["Browser"]
    context.headless = configuration["Headless"]
    context.page_load_timeout = configuration["Page load timeout"]
    context.implicitly_wait = configuration["Implicitly timeout"]

    if context.browser == "Chrome":
        context.options = webdriver.ChromeOptions()
    elif context.browser == "Firefox":
        context.options = webdriver.FirefoxOptions()

    if context.headless == "Yes":
        context.options.add_argument("--headless")


def before_scenario(context, scenario):
    if context.browser == "Chrome":
        driver = webdriver.Chrome(executable_path=driver_loader.get_driver_path(context.browser),
                                  options=context.options)
    elif context.browser == "Firefox":
        driver = webdriver.Firefox(executable_path=driver_loader.get_driver_path(context.browser),
                                   options=context.options)

    driver.set_window_size(1920, 1080)
    driver.set_page_load_timeout(context.page_load_timeout)
    driver.implicitly_wait(context.implicitly_wait)
    context.driver = driver


def after_step(context, step):
    if step.status == "failed":
        allure.attach(context.driver.get_screenshot_as_png(), name="Screenshot", attachment_type=AttachmentType.PNG)


def after_scenario(context, scenario):
    if "cleaner_delete_user_role" in scenario.effective_tags:
        context.execute_steps('''
                When I delete User Role
            ''')
    context.driver.quit()
