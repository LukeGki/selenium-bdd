@functional_tests
Feature: User Roles
  User Roles page tests

  Background:
    Given I am logged in
    And I am on User Roles page

  Scenario: Add user role validation
    When I open Add User Role
    And I click User Role Save button
    Then I should see validation message

  @cleaner_delete_user_role
  Scenario: Add user role
    When I open Add User Role
    And I fill correct Add User Role data
    And I click User Role Save button
    Then I should see correct data in table

  @cleaner_delete_user_role
  Scenario: Edit user role
    Given I added new User Role
    When I open Edit User Role
    And I fill correct Edit User Role data
    And I click User Role Save button
    Then I should see correct data in table

  Scenario: Delete user role
    Given I added new User Role
    When I select User Role checkbox
    And I delete selected rows
    Then I should not see given data in table