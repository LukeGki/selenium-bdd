@smoke_tests
Feature: Login page
  Login page tests

  Scenario: Check page title
    Given I am on login page
    Then I should see correct page title

  Scenario: Log in
    Given I am on login page
    When I log in with correct credentials
    Then I should see correct username on page
