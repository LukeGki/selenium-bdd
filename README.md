# Test automation

## HR Management System

Web application for human resource management

## Scenarios
Smoke tests:
* [Login page](features/login_page.feature)

Functional tests:
* [Main page](features/main_page.feature)
* [User Roles](features/user_roles.feature)

## Test run
1. Get project from VCS
2. Add Interpreter - Create Virtual Environment
3. Install [requirements](requirements.txt)
4. Install web browser and put browser driver into [drivers](drivers) directory
5. Put proper [configuration details](configuration/config_data.json) and [environment details](configuration/env_QA.json) (with password)
6. Install allure-behave with `pip install allure-behave`
7. Run tests:
    * smoke tests with `behave --tags=smoke_tests -f allure_behave.formatter:AllureFormatter -o results ./features`
    * functional tests with `behave --tags=functional_tests -f allure_behave.formatter:AllureFormatter -o results ./features`
8. Download [allure](https://github.com/allure-framework/allure2) and put as [allure](allure) directory
9. Generate report with `allure\bin\allure serve results`

## Environment
* PyCharm v2020.3.2
* Selenium v3.141.0
* Python v3.9.0
* Google Chrome v87
* Windows 10 Pro v20H2