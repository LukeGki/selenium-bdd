from selenium.webdriver.common.by import By
from configuration import config_loader
from pages.base_page import BasePage


class MainPage(BasePage):
    environment = config_loader.load_environment()
    base_url = environment["Base URL"]
    assign_leave_url = base_url + environment["Assign Leave Page URL path"]
    leave_list_url = base_url + environment["Leave List Page URL path"]
    user_roles_url = base_url + environment["User Roles Page URL path"]
    users_url = base_url + environment["Users Page URL path"]
    username = environment["Main Page Username"]

    user_menu_locator = (By.ID, "user-dropdown")
    user_logout_menu_locator = (By.ID, "logoutLink")
    username_locator = (By.ID, "account-name")
    my_shortcuts_menu_trigger_locator = (By.ID, "shortcut-menu-trigger")
    my_shortcuts_menu_locator = (By.ID, "shortcut_menu")
    assign_leave_locator = (By.CSS_SELECTOR, "a[ui-sref='leave.assign']")
    leave_list_locator = (By.CSS_SELECTOR, "a[ui-sref='leave.view_leave_list']")
    admin_menu_locator = (By.ID, "menu_admin_viewAdminModule")
    user_management_locator = (By.ID, "menu_admin_UserManagement")
    user_roles_locator = (By.ID, "menu_admin_viewUserRoles")
    users_locator = (By.ID, "menu_admin_viewSystemUsers")

    def logout(self):
        element = self.driver.find_element(*self.user_menu_locator)
        element.click()
        element = self.driver.find_element(*self.user_logout_menu_locator)
        element.click()

    def get_username(self):
        return self.driver.find_element(*self.username_locator).text

    def get_expected_url(self, option):
        if option == "Assign Leave":
            expected_url = self.assign_leave_url
        elif option == "Leave List":
            expected_url = self.leave_list_url
        elif option == "User Roles":
            expected_url = self.user_roles_url
        elif option == "Users":
            expected_url = self.users_url
        return expected_url

    def click_my_shortcuts_menu(self, option):
        element = self.driver.find_element(*self.my_shortcuts_menu_trigger_locator)
        element.click()
        if option == "Assign Leave":
            locator = self.assign_leave_locator
        elif option == "Leave List":
            locator = self.leave_list_locator
        element = self.driver.find_element(*self.my_shortcuts_menu_locator)
        element = element.find_element(*locator)
        element.click()

    def click_admin_user_management_menu(self, option):
        element = self.driver.find_element(*self.admin_menu_locator)
        element.click()
        element = self.driver.find_element(*self.user_management_locator)
        element.click()
        if option == "User Roles":
            locator = self.user_roles_locator
        elif option == "Users":
            locator = self.users_locator
        element = self.driver.find_element(*locator)
        element.click()
