from selenium.webdriver.common.by import By
from configuration import config_loader
from pages.base_page import BasePage


class LoginPage(BasePage):
    environment = config_loader.load_environment()
    url = environment["Base URL"]
    login_url = url + environment["Login Page URL path"]
    title = environment["Login Page Title"]
    username = environment["Login Page Username"]
    password = environment["Login Page Password"]

    username_input_locator = (By.ID, "txtUsername")
    password_input_locator = (By.ID, "txtPassword")
    login_button_locator = (By.ID, "btnLogin")

    def input_username(self):
        element = self.driver.find_element(*self.username_input_locator)
        element.clear()
        element.send_keys(self.username)
        return self

    def input_password(self):
        element = self.driver.find_element(*self.password_input_locator)
        element.clear()
        element.send_keys(self.password)
        return self

    def click_login_button(self):
        element = self.driver.find_element(*self.login_button_locator)
        element.click()

    def login(self):
        self.input_username()
        self.input_password()
        self.click_login_button()
