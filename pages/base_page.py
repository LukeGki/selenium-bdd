from selenium.webdriver.common.by import By
from configuration import config_loader
from library.helpers import wait_for_element_not_visible


class BasePage:
    configuration = config_loader.load_configuration()
    element_not_visible_timeout = configuration["Element not visible timeout"]

    preloader_locator = (By.ID, "preloader")

    def __init__(self, driver):
        self.driver = driver

    def load(self):
        self.driver.get(self.url)
        return self

    def get_url(self):
        return self.driver.current_url

    def get_title(self):
        return self.driver.title

    def wait_for_preloader_not_visible(self):
        wait_for_element_not_visible(self.driver, self.element_not_visible_timeout,
                                     *self.preloader_locator)
        return self
